// https://docs.cypress.io/api/introduction/api.html

describe('Home Page to Headline', () => {
  beforeEach(() => {
    cy.server();
    cy.route(
      'GET',
      '**/top-headlines*',
      'fixture:get-top-headlines.json'
    );
    cy.visit('/');
  })

  it('visit home page and click "See Headline >>" button', () => {
    cy.wait(1000);
    cy.get('[data-cy=see-headline]').click();
    cy.wait(1000);
  })
})
