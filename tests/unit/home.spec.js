import { shallowMount } from '@vue/test-utils'
import Home from '@/views/Home.vue'

describe('Home.vue', () => {
  it('renders "Welcome to Headliner" when passed', () => {
    const wrapper = shallowMount(Home, {
      stubs: [
        'router-link',
        'v-img',
        'v-container',
        'v-btn'
      ]
    })
    expect(wrapper.text()).toMatch('Welcome to Headliner');
  })
})
