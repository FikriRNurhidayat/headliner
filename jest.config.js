module.exports = {
  coverageReporters: [
    "text-summary",
    "lcov"
  ],
  reporters: [
    "jest-junit"
  ],
  preset: '@vue/cli-plugin-unit-jest'
}
