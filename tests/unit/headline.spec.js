import { shallowMount } from '@vue/test-utils';
import Vue from 'vue';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import Vuetify from 'vuetify';
import Headline from '@/views/Headline.vue';
import responsePayload from './fixtures/responses/get-top-headlines.json';

const mock = new MockAdapter(axios);

describe('Headline.vue', () => {
  beforeAll(() => {
    mock.onGet('/top-headlines').reply(200, responsePayload);
  })
  it('renders list of news when passed', () => {
    Vue.use(Vuetify);
    const wrapper = shallowMount(Headline);
    expect(wrapper.find('v-container-stub').constructor.name).toBe('VueWrapper');
  })
})
