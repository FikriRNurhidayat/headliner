import axios from 'axios';

const token = process.env.VUE_APP_GNEWS_API_KEY;

export const getTopHeadlines = () => axios.get('/top-headlines', {
  params: {
    token
  }
});
