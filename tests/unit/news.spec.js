import { shallowMount } from '@vue/test-utils'
import faker from 'faker';
import News from '@/components/News.vue'

describe('News.vue', () => {
  it('renders article when mounted', () => {
    const propsData = {
      title: faker.hacker.verb(),
      content: faker.lorem.paragraph(),
      url: faker.internet.url(),
      image: faker.image.imageUrl(),
      publishedAt: new Date(),
      source: {
        name: faker.hacker.noun(),
        url: faker.internet.url()
      }
    }
    const wrapper = shallowMount(News, {
      propsData,
      stubs: [
        'router-link',
        'v-btn',
        'v-icon',
        'v-card',
        'v-card-actions',
        'v-img',
        'v-list-item',
        'v-list-item-content',
        'v-list-item-avatar',
        'v-list-item-title',
        'v-list-item-subtitle',
      ]
    })

    const text = wrapper.text();
    expect(text).toMatch(propsData.title);
    expect(text).toMatch(propsData.publishedAt.toString());
    expect(text).toMatch(propsData.content);
    expect(text).toMatch(propsData.source.name);
  })
})
