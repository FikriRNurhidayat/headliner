// https://docs.cypress.io/api/introduction/api.html

describe('Home Page', () => {
  it('visist app root url', () => {
    cy.visit('/');
    cy.contains('h1', 'Welcome to Headliner');
    cy.contains('a', 'See Headline >>');
    cy.get('[data-cy=see-headline]').should('have.attr', 'href', '/headlines');
  })
})
